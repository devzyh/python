import configparser
import logging
import os
import sys


# 配置类
class Config:
    def __init__(self):
        self.doc_id = None
        self.filter_author = None
        self.filter_process = False
        self.filter_keys = ["暂缓", "排期", "暂停", "变更"]
        self.oa_days = 5
        self.tmp_file = "weekly.xlsx"

        self.load_ini()

    # 加载配置
    def load_ini(self):
        config_file = "config.ini"
        if not os.path.exists(config_file):
            logging.error("配置文件[%s]不存在", config_file)
            sys.exit()

        parser = configparser.ConfigParser()
        parser.read(config_file, "UTF-8")
        parser = parser.defaults()

        # 从配置文件中读取配置
        self.doc_id = parser.get("doc_id")
        self.filter_author = parser.get("filter_author")
        self.filter_keys = parser.get("filter_keys").split(",")
        self.oa_days = int(parser.get("oa_days"))


class Work:
    def __init__(self):
        self.project = None
        self.tb_num = None
        self.title = None
        self.hours = 0
        self.author = None
        self.process = 0
        self.remarks = []
        self.priority = None
