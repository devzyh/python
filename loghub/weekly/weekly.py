"""
作者：devzyh
日期：2024-07-12
描述：企业微信在线表格转为邮件内容
"""
import copy
import json
import math
import os
import time
from datetime import datetime, timedelta

import requests
from openpyxl.reader.excel import load_workbook
from openpyxl.worksheet.worksheet import Worksheet
from selenium import webdriver

from loghub.weekly._types import Config, Work


# 下载企业微信表格
def download_excel(cfg: Config):
    driver = webdriver.Edge()
    driver.get("https://doc.weixin.qq.com/smartsheet/" + cfg.doc_id)
    cookies = driver.get_cookies()
    cookie_str = ""
    for cookie in cookies:
        cookie_str = cookie_str + (cookie.get("name") + "=" + cookie.get("value") + "; ")
    print("获取到的Cookies：" + cookie_str)
    driver.quit()

    headers = {"cookie": cookie_str}
    response = requests.post("https://doc.weixin.qq.com/v1/export/export_office?version=2&docId=" + cfg.doc_id,
                             headers=headers)
    if response.status_code != 200:
        print("获取请求操作ID失败")
        exit()
    operation_id = json.loads(response.text).get("operationId")
    print("操作ID：" + operation_id)

    progress = 0
    file_url = ""
    while progress < 100:
        response = requests.get("https://doc.weixin.qq.com/v1/export/query_progress?operationId=" + operation_id,
                                headers=headers)
        if response.status_code != 200:
            print("获取文件下载地址失败")
            exit()

        data = json.loads(response.text)
        file_url = data.get("file_url")
        progress = data.get("progress")
        if progress < 100:
            time.sleep(1)

    print("文件下载地址：" + file_url)

    # 导出到临时文件
    response = requests.get(file_url)
    if response.status_code != 200:
        print("导出文件下载失败")
        exit()

    with open(cfg.tmp_file, "wb") as f:
        f.write(response.content)
    print("保存内容到临时文件：" + cfg.tmp_file)


# 读取工作簿内容
def read_sheet(ws: Worksheet) -> list[Work]:
    works = []
    row_index = 2
    while row_index <= ws.max_row:
        work = Work()
        work.project = ws.cell(row=row_index, column=1).value

        work.tb_num = ws.cell(row=row_index, column=5).value

        work.title = ws.cell(row=row_index, column=6).value
        if work.tb_num is not None:
            work.title = str(work.title).strip().replace("\n", " ")

        work.hours = ws.cell(row=row_index, column=7).value
        if work.hours is None:
            work.hours = 1

        work.author = ws.cell(row=row_index, column=8).value

        work.process = ws.cell(row=row_index, column=9).value
        if work.process is None:
            work.process = 0

        work.remarks = ws.cell(row=row_index, column=10).value
        if work.remarks is None:
            work.remarks = []
        else:
            work.remarks = str(work.remarks).strip().splitlines()

        work.priority = ws.cell(row=row_index, column=11).value

        row_index += 1
        works.append(work)

    return works


# 过滤内容
def filter_works(cfg: Config, works: list[Work]) -> list[Work]:
    new_works = []
    for work in works:
        if cfg.filter_author is not None:
            if cfg.filter_author != work.author:
                continue

        if cfg.filter_process:
            if work.process is None or work.process <= 0:
                continue
            else:
                work.hours = math.ceil(work.hours * work.process)

        if cfg.filter_keys is not None:
            skip = False
            for key in cfg.filter_keys:
                for remark in work.remarks:
                    if key in remark:
                        skip = True
                        break

                if skip:
                    break

                if work.priority is not None and key in work.priority:
                    skip = True
                    break

            if skip:
                continue

        new_works.append(work)

    return new_works


# 输出邮件内容
def output_mail(works: list[Work], title: str):
    # 项目分组
    project_works = {}
    for work in works:
        p_works = []
        if work.project in project_works:
            p_works = project_works.get(work.project)

        p_works.append(work)

        project_works[work.project] = p_works

    print("{}：".format(title))
    for p_name in project_works:
        print("    {}：".format(p_name))
        for work in project_works[p_name]:
            tb_num = ""
            if work.tb_num is not None:
                tb_num = work.tb_num + "："

            if len(work.remarks) == 0:
                print("        {}{}".format(tb_num, work.title))
            elif len(work.remarks) == 1:
                print("        {}{} {}".format(tb_num, work.title, work.remarks[0]))
            else:
                print("        {}{}：".format(tb_num, work.title))
                for remark in work.remarks:
                    print("            {}".format(remark))

    print("")


# 输出OA工时
def output_oa(cfg: Config, works: list[Work]):
    items = []
    hours = 0
    for work in works:
        if len(work.remarks) == 0:
            items.append(work)
            hours += work.hours
            continue

        if len(work.remarks) == 1:
            work.title += work.remarks[0]
            items.append(work)
            hours += work.hours
            continue

        for remark in work.remarks:
            w = copy.deepcopy(work)
            w.title = work.title + " " + remark
            w.hours = math.ceil(work.hours / len(work.remarks))
            items.append(w)
            hours += w.hours

    items = sorted(items, key=lambda w: w.project)
    day_hours = math.ceil(hours / cfg.oa_days)

    print("\n本周共计{}小时，平均每天{}小时\n".format(hours, day_hours))
    print("日期\t项目\t工时\t内容")
    print("-------------------")

    row_date = datetime.now() - timedelta(days=cfg.oa_days + 1)
    total_hours = 0
    index = 0
    for item in items:
        if total_hours == 0:
            row_date = row_date + timedelta(days=1)

        # 拆分跨天
        more_hours = item.hours - day_hours + total_hours
        if more_hours > 0:
            item.hours = day_hours - total_hours

            itm = copy.deepcopy(item)
            itm.hours = more_hours
            items.insert(index + 1, itm)

        title = item.title
        if item.tb_num is not None:
            title = item.tb_num + " " + title

        print("{}\t{}\t{}\t{}".format(row_date.strftime('%Y-%m-%d'), item.project, item.hours, title))

        total_hours += item.hours
        if total_hours == day_hours:
            total_hours = 0
            print("-------------------")

        index += 1


# 主程序
print("OMS技术周报生成器v1.1")
cfg = Config()

download_excel(cfg)

# 读取excel文件
print("开始提取文件[{}]内容".format(cfg.tmp_file))
wb = load_workbook(filename=cfg.tmp_file, read_only=True)

print("================================生成内容仅供参考，请根据实际情况进行修改================================")

# 总结
c_week = read_sheet(wb.worksheets[1])
cfg.filter_process = True
c_week = filter_works(cfg, c_week)
output_mail(c_week, "总结")

# 计划
n_week = read_sheet(wb.worksheets[0])
cfg.filter_process = False
n_week = filter_works(cfg, n_week)
output_mail(n_week, "计划")

print("====================================内容已生成完毕，请复制到邮件发送====================================")

output_oa(cfg, c_week)
print("====================================内容已生成完毕，请复制OA系统提交====================================")

wb.close()
os.remove(cfg.tmp_file)
print("文件[{}]已删除".format(cfg.tmp_file))
input("Press Enter to continue...")
