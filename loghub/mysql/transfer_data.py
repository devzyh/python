import threading
import time
from concurrent.futures import ThreadPoolExecutor, as_completed
from datetime import datetime
from pathlib import Path

import mysql.connector
from sshtunnel import SSHTunnelForwarder

# SSH和MySQL配置信息
SSH_HOST = '127.0.0.1'
SSH_PORT = 22
SSH_USERNAME = 'root'
SSH_PASSWORD = 'root'

MYSQL_HOST = 'xxxx.mysql.zhangbei.rds.aliyuncs.com'
MYSQL_PORT = 3306
MYSQL_USER = 'test'
MYSQL_PASSWORD = 'test'
MYSQL_DB = 'test'

# 表名列表及其对应的起始ID
TABLES = [
    {"table_name": "transaction_history", "start_id": "788901934"},
]

# 每批次转移数量
TRANSFER_LIMIT = 300000
print(f"TRANSFER_LIMIT: {TRANSFER_LIMIT}")

# 根据Mysql服务器性能调整
MAX_THREADS = len(TABLES)
print(f"MAX_THREADS: {MAX_THREADS}")


# 创建SSH隧道
def create_ssh_tunnel():
    tunnel = SSHTunnelForwarder(
        (SSH_HOST, SSH_PORT),
        ssh_username=SSH_USERNAME,
        ssh_password=SSH_PASSWORD,
        remote_bind_address=(MYSQL_HOST, MYSQL_PORT),
    )
    return tunnel


# 连接到MySQL数据库
def connect_to_mysql(tunnel):
    connection = mysql.connector.connect(
        host='127.0.0.1',
        port=tunnel.local_bind_port,
        user=MYSQL_USER,
        password=MYSQL_PASSWORD,
        database=MYSQL_DB,
    )
    return connection


# 转移单表数据
def transfer_table(table_info, tunnel):
    table_name = table_info['table_name']
    tmp_table_name = table_name + "_pytmp"
    start_id = int(table_info['start_id'])

    try:
        connection = connect_to_mysql(tunnel)
        cursor = connection.cursor()

        # 创建表
        create_sql = f"CREATE TABLE IF NOT EXISTS {tmp_table_name} LIKE {table_name}"
        cursor.execute(create_sql)
        connection.commit()

        max_id_sql = f"select max(id) from {tmp_table_name}"
        cursor.execute(max_id_sql)
        max_id = cursor.fetchone()[0]
        if max_id is not None:
            start_id = max_id
        if start_id is None:
            start_id = 0

        while True:
            start_time = datetime.now()

            end_id = start_id + TRANSFER_LIMIT
            transfer_sql = f"INSERT INTO {tmp_table_name} SELECT * FROM {table_name} WHERE id > {start_id} and id <= {end_id}"
            cursor.execute(transfer_sql)
            affected_rows = cursor.rowcount
            connection.commit()

            end_time = datetime.now()
            duration = end_time - start_time

            if affected_rows == 0:
                break

            print(
                f"Thread-{threading.get_ident()}: Transfer {affected_rows} rows from {table_name} duration: {duration}")

            # 退出标识文件
            if Path("shutdown").exists():
                break

            # 下一轮起始ID
            cursor.execute(max_id_sql)
            start_id = cursor.fetchone()[0]

        cursor.close()
        connection.close()
        print(f"Thread-{threading.get_ident()}: Finished transfer from {table_name}")
    except Exception as e:
        print(f"Error in thread for table {table_name}: {e}")
    finally:
        pass


# 主函数
def main():
    start_time = datetime.now()
    print(f"Program started at {start_time.strftime('%Y-%m-%d %H:%M:%S')}")

    try:
        # 创建SSH隧道
        with create_ssh_tunnel() as tunnel:
            tunnel.start()
            print("SSH tunnel established.")

            # 使用线程池并发执行分批删除
            with ThreadPoolExecutor(max_workers=MAX_THREADS) as executor:
                futures = []
                for i, table_info in enumerate(TABLES):
                    future = executor.submit(transfer_table, table_info, tunnel)
                    print(f"Task Index: {i} Submit")
                    time.sleep(3)
                    futures.append(future)

                # 等待所有线程完成
                for future in as_completed(futures):
                    try:
                        future.result()
                    except Exception as exc:
                        print(f"Thread generated an exception: {exc}")

            print("All threads completed.")
    finally:
        tunnel.close()

        end_time = datetime.now()
        duration = end_time - start_time
        print(f"Program completed at {end_time.strftime('%Y-%m-%d %H:%M:%S')}")
        print(f"Total duration: {duration}")


if __name__ == "__main__":
    main()
