import sys
import time

import paramiko
import yaml

# 加载配置文件
with open("config.yml", "r", encoding="utf-8") as file:
    data = yaml.safe_load(file)
if data is None:
    print("配置文件[config.yaml]不存在")
    sys.exit()

hosts = data["hosts"]
commands = data["commands"]

# 获取执行的命令
args = sys.argv
if len(args) == 1:
    print("执行的命令不能为空")
    sys.exit()
spec_index = -1
if len(args) > 2:
    spec_index = int(args[2])
    print("指定命令步骤[{}]".format(spec_index))

command = args[1]

if command not in commands:
    print("执行的命令[{}]不存在".format(command))
    sys.exit()
command_info = commands[command]

print("开始执行命令[{}]的步骤".format(command))

# 默认配置
default_host = None
if "host" in command_info:
    default_host = command_info["host"]
default_action = None
if "action" in command_info:
    default_action = command_info["action"]
if "setups" not in command_info:
    print("命令[{}]的步骤未配置，退出执行".format(command))
setups = command_info["setups"]

index = 0
if spec_index > 0:
    index = spec_index
last_host = ""
ssh = paramiko.SSHClient()
ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
while index < len(setups):
    print("============================================")
    print("开始执行步骤[{}]".format(index))
    setup = setups[index]
    index += 1

    # 命令
    bash = None
    if "setup" in setup:
        bash = setup["setup"]
    # 主机
    host = None
    if "host" in setup:
        host = setup["host"]
    # 执行结果表达式
    result = None
    if "result" in setup:
        result = setup["result"]
    # 失败后动作
    action = None
    if "action" in setup:
        action = setup["action"]

    # 默认值
    if host is None:
        host = default_host
    if action is None:
        action = default_action
    if result is None:
        result = "*"
    if result is None or action is None:
        action = "ignore"

    if bash is None or host is None:
        continue

    print("步骤配置信息：\nsetup:{}\nhost:{}\nresult:{}\naction:{}".format(bash, host, result, action))

    # 连接主机
    if last_host != host:
        ssh.close()
        print("尝试连接主机[{}]".format(host))
        host_info = hosts[host]
        ssh.connect(host_info["host"], port=host_info["port"], username=host_info["user"], password=host_info["pass"])
        print("主机[{}]连接成功".format(host))

    # 执行命令
    stdin, stdout, stderr = ssh.exec_command(bash)

    # 获取命令结果
    output = stdout.read().decode()
    error_output = stderr.read().decode()
    if error_output:
        output = error_output
    print("命令执行结果：{}".format(output))

    # 结果验证
    success = True
    key = result.replace("*", "")
    if result.startswith("*") and not output.endswith(key):
        success = False
    if result.endswith("*") and not output.startswith(key):
        success = False
    if result.startswith("*") and result.endswith("*") and key not in output:
        success = False
    if "*" not in result and result != output:
        success = False

    if success:
        print("当前步骤执行完毕")
        continue

    # 失败后处理
    if action == "ignore":
        print("验证结果失败，忽略错误")
        continue
    if action == "exit":
        print("验证结果失败，退出命令")
        break
    if action.startswith("sleep "):
        seconds = int(action.replace("sleep ", ""))
        print("验证结果失败，睡眠[{}]秒".format(seconds))
        time.sleep(seconds)
        index -= 1
        continue

# 关闭最后一个连接
ssh.close()
print("============================================")
print("命令[{}]的步骤执行完毕".format(command))
